package ru.ezforus.rational;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Выполняет все математические операции с выражением, в виде дроби.
 *
 * @author - Никита, Артем
 * @version 0.2
 */
public class Rational {
    private int intPart;
    private int numerator;
    private int denominator;

    public Rational(int numerator, int denominator,int intPart) {
        this.intPart = intPart;
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Rational(int numerator, int denominator) {
        intPart = 0;
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Rational() {
        intPart = 0;
        numerator = 1;
        denominator = 1;
    }

    public int getIntPart() {
        return intPart;
    }

    public void setIntPart(int intPart) {
        this.intPart = intPart;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    /**
     * Превращает строку с числом в объект дроби
     *
     * @author - Никита
     * @param number - число
     * @return - число в виде объекта
     */
    protected static Rational convertToRational(String number){
        int denominator,numerator;
        Pattern pattern = Pattern.compile("/");
        Matcher matcher = pattern.matcher(number);

        if(matcher.find()) {
            String[] numeratorAndDenominator = number.split("/");
            numerator = Integer.parseInt(numeratorAndDenominator[0]);
            denominator = Integer.parseInt(numeratorAndDenominator[1]);
        } else {
            numerator = Integer.parseInt(number);
            denominator = 1;
        }

        Rational rational = new Rational(numerator,denominator);
        return rational;
    }

    /**
     * Умножение дробей
     *
     * @author - Никита
     * @param firstNumber - первая дробь
     * @param secondNumber - вторая дробь
     * @return - результат умножения
     */
    protected static Rational multiplication(Rational firstNumber, Rational secondNumber){
        int numerator = firstNumber.numerator * secondNumber.numerator;
        int denominator = firstNumber.denominator * secondNumber.denominator;
        Rational result = new Rational(numerator,denominator);

        return result;
    }

    /**
     * Деление дробей
     *
     * @author - Никита
     * @param firstNumber - первая дробь
     * @param secondNumber - вторая дробь
     * @return - результат деления
     */
    protected static Rational division(Rational firstNumber, Rational secondNumber){
        int numerator = firstNumber.numerator * secondNumber.denominator;
        int denominator = firstNumber.denominator * secondNumber.numerator;
        Rational result = new Rational(numerator,denominator);

        return result;
    }

    /**
     * Сложение дробей
     *
     * @author - Никита
     * @param firstNumber - первая дробь
     * @param secondNumber - вторая дробь
     * @return - результат сложения
     */
    protected static Rational addition(Rational firstNumber, Rational secondNumber){
        if(firstNumber.denominator != secondNumber.denominator)
            commonDenominator(firstNumber,secondNumber);
        int numerator = firstNumber.numerator + secondNumber.numerator;
        int denominator = firstNumber.denominator;
        Rational result = new Rational(numerator,denominator);

        return result;
    }

    /**
     * Деление дробей
     *
     * @author - Никита
     * @param firstNumber - первая дробь
     * @param secondNumber - вторая дробь
     * @return - результат вычитания
     */
    protected static Rational subtraction(Rational firstNumber, Rational secondNumber){
        if(firstNumber.denominator != secondNumber.denominator)
            commonDenominator(firstNumber,secondNumber);
        int numerator = firstNumber.numerator - secondNumber.numerator;
        int denominator = firstNumber.denominator;
        Rational result = new Rational(numerator,denominator);

        return result;
    }

    /**
     * Приводит дроби к общему знаменателю
     *
     * @author - Никита
     * @param firstNumber - первая дробь
     * @param secondNumber - вторая дробь
     */
    protected static void commonDenominator(Rational firstNumber, Rational secondNumber){
        firstNumber.setNumerator(firstNumber.numerator*secondNumber.denominator);
        secondNumber.setNumerator(secondNumber.numerator*firstNumber.denominator);
        int firstDenominator = firstNumber.denominator;
        firstNumber.setDenominator(firstNumber.denominator*secondNumber.denominator);
        secondNumber.setDenominator(secondNumber.denominator*firstDenominator);
    }

    /**
     * Сокращении дроби с выделением целой части
     *
     * @author - Никита
     * @param rational - дробь
     * @return - упрощенная дробь
     */
    protected static Rational reduction(Rational rational){
        int intPart = rational.numerator / rational.denominator;
        int numerator = rational.numerator - rational.denominator;
        int demonimator = rational.denominator;
        Rational result = new Rational(numerator,demonimator,intPart);

        return result;
    }

    @Override
    public String toString() {
        return +intPart+" "+numerator+"/"+denominator;
    }
}


