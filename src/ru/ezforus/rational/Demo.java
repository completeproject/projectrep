package ru.ezforus.rational;

import java.util.Scanner;

/**
 * Демонстрирует работу класса Rational и выполняет обработку строки выражения для дальнейшего его решения.
 *
 * @author - Никита, Артём
 * @version 0.2
 */
public class Demo {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Rational example = new Rational();
        Rational answerToExample;

        String expression = inputExpression(scanner);
        expression = replaceForSplit(expression);
        String[] numbers = expression.split("\\+|\\*|:|-");
        String[] operations = expression.split("(minus|\\d)([/]?\\d)?");
        numbers = replaceForWork(numbers);
        answerToExample = examplesOfSolutions(numbers, operations, example);
        toString(answerToExample);
    }

    /**
     * Позволяет совершить ввод выражение с клавиатуры
     *
     * @author - Никита
     * @return - выражение в виде строки
     */
    public static String inputExpression(Scanner scanner){
        String expression;

        System.out.println("Введите желаемое выражение, используя следующие символы.");
        System.out.println("+ сложение");
        System.out.println("- вычитание");
        System.out.println("* умножение");
        System.out.println(": деление");
        System.out.println("число/число дробь");
        System.out.println("Пример: -2*3+3/4:5-5*(-5)");
        System.out.print("Ввод: ");

        expression = scanner.nextLine();
        testExpression(expression, scanner);

        return expression;
    }

    /**
     * Выполняет решение примера
     *
     * @author - Никита
     * @param numbers - все числа выражения
     * @param operations - все математические операторы выражения
     * @param example - объект выражения
     */
    public static Rational examplesOfSolutions(String[] numbers, String[] operations, Rational example){
        Rational resultOfExample;
        Rational[] answerToMultiplication;

        answerToMultiplication = solutionMultiplication(numbers, operations, example);
        resultOfExample = solutionAdditional(numbers, operations, example, answerToMultiplication);
        resultOfExample.reduction(resultOfExample);

        return resultOfExample;
    }

    /**
     * Выполняет решене всех сложений и вычитаний в выражении
     *
     * @author - Никита
     * @param numbers - все числа выражения
     * @param operations - все математические операторы выражения
     * @param example - объект выражения
     * @param answerToMultiplication - результаты умножения/деления в выражении
     * @return - результат всех сложений и вычитаний в данном выражении
     */
    private static Rational solutionAdditional(String[] numbers, String[] operations, Rational example, Rational[] answerToMultiplication){
        String number;
        int j = 0;
        Rational firstNumber = new Rational();
        Rational secondNumber = new Rational();
        Rational result = new Rational(0,0);
        for(int i = 0; i<=operations.length; i++){
            if(operations[i] == "+" || operations[i] == "-"){
                number = numbers[--i];
                numbers[--i] = "";
                if(number == "")
                    firstNumber = answerToMultiplication[j];
                else
                    firstNumber = example.convertToRational(number);
                number = numbers[i];
                numbers[i] = "";
                if(number == "")
                    secondNumber = answerToMultiplication[++j];
                else
                    secondNumber = example.convertToRational(number);
            }
            if(operations[i] == "+") {
                result = example.addition(result, firstNumber);
                result = example.addition(result, secondNumber);
            } else if (operations[i] == "-"){
                result = example.subtraction(result,firstNumber);
                result = example.subtraction(result,secondNumber);
            }
        }
        return result;
    }

    /**
     * Выполняет решение всех умножений и делений в выражении
     *
     * @author - Никита
     * @param numbers - все числа выражения
     * @param operations - все математические операторы выражения
     * @param example - объект выражения
     * @return - ответы на каждое деление/умножение в выражении
     */
    private static Rational[] solutionMultiplication(String[] numbers, String[] operations, Rational example){
        String number;
        int j = 0;
        Rational firstNumber = new Rational();
        Rational secondNumber = new Rational();
        Rational answerToMultiplication[] = new Rational[numbers.length];
        for(int i = 0; i<=operations.length; i++){
           if(operations[i] == "*" || operations[i] == ":"){
               number = numbers[--i];
               numbers[--i] = "";
               firstNumber = example.convertToRational(number);
               number = numbers[i];
               numbers[i] = "";
               secondNumber = example.convertToRational(number);
               if(operations[i] == "*")
                answerToMultiplication[j++] = example.multiplication(firstNumber, secondNumber);
               else if(operations[i] == ":")
                 answerToMultiplication[j++] = example.division(firstNumber,secondNumber);
               operations[i] = "";
           }
        }
        return answerToMultiplication;
    }


    /**
     * Заменяет минусы НЕ являющиеся математическим оператором
     *
     * @author - Никита
     * @param expression - строка выражения
     * @return - преобразованная строка выражения
     */
    private static String replaceForSplit(String expression){
        expression = expression.replace("*(-", "*minus");
        expression = expression.replace(":(-",":minus");
        expression = expression.replace(")","");

        if(expression.startsWith("-"))
            expression = expression.replaceFirst("-","minus");

        return expression;
    }

    /**
     * Совершает преобразование строки в рабочий вид
     *
     * @author - Никита
     * @param expression - строка выражения
     * @return - преобразованная строка выражения
     */
    private static String[] replaceForWork(String[] expression){
        String  resultAfterReplace;
        String[] resultAfterReplaceInMassive = new String[expression.length];
        for(int i = 0; i<=expression.length; i++){
            resultAfterReplace = expression[i].replace("minus", "-");
            resultAfterReplaceInMassive[i] = resultAfterReplace;
        }
        return resultAfterReplaceInMassive;
    }

    /**
     * Проверяет выражение на наличие в нём букв и прочих ошибок
     *
     * @author - Никита
     * @param expression - строка выражение
     */
    // Не до конца реализованная функция. ДОДЕЛАЙ
    private static void testExpression(String expression, Scanner scanner){
        while(expression.matches("[a-zа-яё]+")){
            System.out.println("Вы ввели буквы в выражении.");
            System.out.println("Не надо так!");
            System.out.print("Введите заново: ");
            expression = scanner.next();
        }
    }

    /**
     * Выводит на монитор ответ выражения
     *
     * @auhtor - Никита
     * @param example - объект выражения
     * @return - строка с результатом решения выражения
     */
    public static String toString(Rational example) {
        return "Ответ: " + example.toString();
    }
}
